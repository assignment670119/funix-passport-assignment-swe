# **THIẾT KẾ FUNIX PASSPORT**

![](picture/media/image1.png)

## **Giới thiệu tổng quan về dự án.**

1.  **Tóm tắt dự án.**

-   Thông tin cơ bản về dự án:

-   Tên dự án: Hệ thống dịch thuật cho học viên tại Funix(Funix Passport).

-   Mục đích của dự án: là để giúp cho học viên dễ dàng nắm được các kiến thức từ video hơn thay vì phải xem phụ đề Tiếng Anh có sẵn.

-   Mục tiêu của dự án: là làm cho học viên có thể hiểu khi xem video hoặc đọc các tài liệu trên MOOC vì thế cần có riêng một đội ngũ **Translator** để tạo bài dịch cho những video hoặc tài liệu, đội ngũ Reviewer để kiểm duyệt các bài dịch đã được upload, và có **Admin** để quản lý các bản dịch và tài khoản trong hệ thống.

-   Funix Passport sẽ giải quyết những vấn đề như: dịch các thuật ngữ chuyên ngành, từ khóa, ... của một video hay 1 tài liệu từ tiếng anh sang tiếng việt sau đo kiểm duyệt và sinh viên có thể xem được các tài liệu đó ở Tiếng Việt (có thể là phụ đề cho video hoặc một trang tài liệu).

2.  **Phạm vi của dự án.**

Phạm vi của dự án:

+   Phạm vi về khách hàng: Khánh hàng đa số sẽ là những học sinh, sinh viên trong FUNIX, thậm chí có thể là các mentor, tutor, ...

+   Phạm vi về nền tảng: có thể hoạt động trong Chrome và các hệ điều hành như Window, Linux, Mac OS, Android, ....

## **Yêu cầu và đặc tả dự án.**

###  **Yêu cầu chức năng.**

1.  **Yêu cầu chức năng dàng cho User:**

+   User sẽ đăng nhập và đăng xuất khỏi ứng dụng.

2.  **Yêu cầu chức năng dàng cho Reviewer:**

+   Reviewer có các chức năng mà User có.

+   Reviewer có thể kiểm tra Translator đã dịch đúng chưa.

+   Reviewer có thể kiểm soát cho hoặc không cho hiển thị bài dịch

3.  **Yêu cầu chức năng dàng cho Translator:**

+   Translator có các chức năng mà User có.

+   Translator có thể upload các bản dịch lên hệ thống.

+   Translator có thể download các bản dịch từ hệ thống.

+   Translator có thể xem danh sách các bản dịch đã có trên hệ thống.

+   Translator có thể tìm kiếm các bản dịch theo bộ lọc.

+   Translator có thể tạo các bản dịch(file phụ đề hoặc file document).

+   Translator có thể xóa các bản dịch(file phụ đề hoặc file document).

+   Translator có thể chỉnh sửa thông tin của các bản dịch(file phụ đề hay document).

4.  **Yêu cầu chức năng dàng cho Admin:**

+   Admin có các chức năng mà User có.

+   Admin có thể cấp phép cho Reviewer để duyệt bài dịch.

+   Admin có thể xóa tài khoản người dùng.

+   Admin có thể thêm quyền cho người dùng (User -\> Translator).

+   Admin có thể xóa quyền cho người dùng (Translator -\> User).

###  **Yêu cầu phi chức năng.**

-   Giao diện của hệ thống được xây dựng dễ hiểu, người dùng không cần biết quá nhiều về công nghệ cũng có thể sử dụng hệ thống.

-   Extension được xây dựng để sử dụng chủ yếu trên trình duyệt Chrome.

1.  **Tính bảo mật.**

-   Phải có tính bảo mật cao, dữ liệu được lưu trữ ở Database an toàn. Đồng thời bạn cũng cần các API Key hay Token mới có thể truy cập và chỉnh sửa dữ liệu ở Database.

-   Cần sử dụng số điện thoại để sát thực tài khoản đi tạo tài khoản, thay đổi mật khẩu hoặc thông tin cá nhân.

-   Mỗi tài khoảng người dùng đều phải có mật khẩu cấp Normal trở lên.

2.  **Tính sẵn sàng và khả năng đáp ứng.**

-   Hệ thống phải đảm bảo vận hành 24/7 và không phụ thuộc vào sự ngưng hoạt động của bất kỳ sản phẩm đến từ bên thứ 3 nào.

-   Tên người dùng không có ký tự đặc biệt.

-   Không dùng space trong tên người dùng, ID của file phụ đề và ID của file document mà thay bằng "\_".

-   Khi tải/xuất file phụ đề hoặc file document thì dạng file đó phải có đuôi .docx

3.  **Hiệu suất.**

-   Độ trể khi hiển thị bảng popup cho người dùng để lựa chọn cần hay không cần bản dịch không quá **1s.**

-   Có tốc độ ổn định, thời gian để hiển thị bản dịch tính từ khi học viên vào Website không được quá **1s.**

-   Thời gian để submit và thực hiện các thao tác của Translator cũng cần phải có tốc độ xử lý nhanh, trung bình mỗi thao tác không được quá **0.5s**.

###  **Đặc tả phần mềm.**

-   Hệ thống phải đảm bảo vận hành 24/7.

-   Hệ thống phải hổ trợ tốt việc đưa các bản dịch của Translator lên các video hoặc tài liệu ở các MOOC.

-   Ứng dụng được lập trình bằng ngôn ngữ Java cho hệ điều hành Win, Linux, ...

-   Ứng dụng được xây dựng để sử dụng chủ yếu trên trình duyệt Chrome vì các MOOC đa số ở trình duyệt Chrome.

-   Giao diện phải thể hiện bằng tiếng Việt.

## **Kiến trúc và thiết kế phần mềm.**

1.  **Kiến trúc phần mềm.**

-   Mẫu kiến trúc phần mềm phù hợp với dự án là mẫu Client-Server. Vì Translator đưa bản dịch vào dự án(nghĩa là ở Client) thì sau khi được Reviewer xác nhận thì sẽ được cấp phép được lưu vào database(tức là Server) để hiện thị bản dịch khi học viên cần. 

2.  **Usecase Diagram.**
![](picture/media/image2.png)

3.  **Class Diagram.**
![](picture/media/image3.png)

4.  **Sequence Diagram.**
![](picture/media/image4.png)

5.  **Activity Diagram.**

![](picture/media/image5.png)
